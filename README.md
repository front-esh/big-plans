Let this be the place where we get to know each other's 
- **p**lans
- **e**ndeavors
- **a**ccomplishments

>  Knowledge is a graph.

We will use this place to organise related things, plan to learn ideas and concepts, and share what we build.

## Seasons

We can broadly divide the endeavors we have to undertake into themes or *seasons*. A season can extend upto a quarter.
This will help us to constrain ourselves to a prioritised, well thought list of things to undertake.

*Season 1 - React Ecosystem*

*Season 2 - Progressive Web Apps, Web Performance*

*Season 3 - Elm, Other friends of React*

## Workflow

The most important part of this project will be the issue boards.

We will re-purpose them to track our progress in a season and hold ourselevs accountable to others.

And there are some **rules**.

There are three labels which are *listed* in the issue board

-  Concepts
-  Resources
-  Efforts

**Concept** should enlist bigger ideas/framework we intend to learn like VueJS, mobx, etc.

**Resources** are the actual learning materials. It can be a video course, article, stack overflow discussion etc.

**Efforts** are about content, code or projects created by us to demonstrate our learning or build something useful.

We must take care so that each issue is assigned the right label above. In case of ambiguity it is better to have a discussion with the team.







 